<%@ page import="model.entity.RealPerson" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>updated customer showing room</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<h1>Update Done</h1>
<br>
<br>
<div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Family</th>
            <th>FatherName</th>
            <th>BirthDate</th>
            <th>NationalCode</th>
            <th>CustomerNumber</th>
        </tr>
        </thead>
        <tbody>
        <%
            RealPerson realPerson = (RealPerson) request.getAttribute("updateResult");
        %>
        <tr>
            <td><%=realPerson.getName()%>
            </td>
            <td><%=realPerson.getFamily()%>
            </td>
            <td><%=realPerson.getFatherName()%>
            </td>
            <td><%=realPerson.getBirthDate()%>
            </td>
            <td><%=realPerson.getNationalCode()%>
            </td>
            <td><%=realPerson.getCustomerNumber()%>
            </td>
        </tr>
        </tbody .table-hover>
    </table>
</div>
<form action="/person/searchCustomers/searchCustomers.jsp">
    <button type="submit" class="btn btn-primary">Home</button>
</form>
</body>