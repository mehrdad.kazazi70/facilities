<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>search</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div>
<ul class="nav nav-tabs">
    <li role="presentation"><a href="/person/definitionCustomer/definitionProfile.jsp">New customer definition</a></li>
    <li role="presentation"><a href="/person/searchCustomers/searchCustomers.jsp">Search Customer</a></li>
    <li role="presentation"><a href="/loan/definitionLoanType/loanType.jsp">Definition LoanType</a></li>
    <li role="presentation"><a href="/loan/definitionLoanType/filing.jsp">Filing</a></li>
</ul>
</div>
<div class="container">
    <form class="form-horizontal" action="/realPerson/findAll.do">
        <fieldset>
            <legend>Search RealCustomer</legend>
            <div class="glyphicon glyphicon-search"></div>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realName">name</label>
                <div class="col-md-4">
                    <input id="realName" name="realName" type="text" placeholder="name" class="form-control input-md" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realFamily">family</label>
                <div class="col-md-4">
                    <input id="realFamily" name="realFamily" type="text" placeholder="family"
                           class="form-control input-md" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realNationalCode">nationalCode</label>
                <div class="col-md-4">
                    <input id="realNationalCode" name="realNationalCode" type="text" value="" minlength="8" maxlength="10" placeholder="nationalCode"
                           class="form-control input-md" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realCustomerNumber">customerNumber</label>
                <div class="col-md-4">
                    <input id="realCustomerNumber" name="realCustomerNumber" type="text" placeholder="customerNumber"
                           class="form-control input-md" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton2"></label>
                <div class="col-md-4">
                    <button id="singlebutton2" name="singlebutton" class="btn btn-primary">Search</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>