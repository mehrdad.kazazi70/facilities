<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>definition Customer</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div>
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/person/definitionCustomer/definitionProfile.jsp">New customer definition</a>
        </li>
        <li role="presentation"><a href="/person/searchCustomers/searchCustomers.jsp">Search Customer</a></li>
        <li role="presentation"><a href="/loan/definitionLoanType/loanType.jsp">Definition LoanType</a></li>
        <li role="presentation"><a href="/loan/definitionLoanType/filing.jsp">Filing</a></li>
    </ul>
</div>
<div class="container">
    <form class="form-horizontal" action="/realPerson/save.do" id="frm1"
          onsubmit=" return isValidIranianNationalCode()">
        <fieldset>
            <legend>RealCustomer Profile</legend>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realName">name</label>
                <div class="col-md-4">
                    <input id="realName" name="name" type="text" placeholder="name" class="form-control input-md"
                           required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realFamily">family</label>
                <div class="col-md-4">
                    <input id="realFamily" name="family" type="text" placeholder="family"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realFatherName">fatherName</label>
                <div class="col-md-4">
                    <input id="realFatherName" name="fatherName" type="text" placeholder="fatherName"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realBirthDate">birthDate</label>
                <div class="col-md-4">
                    <input id="realBirthDate" data-format="yyyy-mm-dd" name="birthDate" type="date"
                           placeholder="yyyy-mm-dd"
                           class="form-control input-md" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="realNationalCode">nationalCode</label>
                <div class="col-md-4">
                    <input id="realNationalCode" name="nationalCode" type="text" value="" maxlength="10"
                           placeholder="nationalCode"
                           class="form-control input-md">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton1"></label>
                <div class="col-md-4">
                    <input type="submit" id="singlebutton1" name="singlebutton" class="btn btn-primary" value="submit">
                </div>
            </div>
            <div class="alert alert-danger" style="opacity:0;transition: 0.7s" role="alert">
                <strong>Error!</strong>
            </div>
        </fieldset>
    </form>
</div>

<script>
    var name = document.getElementById("realName");
    var family = document.getElementById("realFamily");
    var fatherName = document.getElementById("realFatherName");
    var birthDate = document.getElementById("realBirthDate");
    var nationalCode = document.getElementById("realNationalCode");
    var error_box = document.getElementsByClassName("alert alert-danger")[0];

    function isValidIranianNationalCode() {
        if (name.value === "" || family.value === "" || fatherName.value === "" || birthDate.value === "" || nationalCode.value === "") {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'please fill the blank fields !';
            return false;
        } else if (/[^0-9]/g.test(nationalCode.value)) {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'please enter only number in your nationalCode field !';
            return false;
        } else if (nationalCode.value.length < 10) {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'your nationalCode in too short !';
            return false;
        } else if (nationalCode.value === '0000000000' || nationalCode.value === '1111111111' || nationalCode.value === '2222222222' || nationalCode.value === '3333333333' || nationalCode.value === '4444444444' || nationalCode.value === '5555555555' || nationalCode.value === '6666666666' || nationalCode.value === '7777777777' || nationalCode.value === '8888888888' || nationalCode.value === '9999999999') {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += '  nationalCode is invalid!';
            return false;
        } else if (nationalCode.value) {
            var check = parseInt(nationalCode.value[9]);
            var sum = 0;
            var i;
            for (i = 0; i < 9; ++i) {
                sum += parseInt(nationalCode.value[i]) * (10 - i);
            }
            sum %= 11;
            return (sum < 2 && check === sum) || (sum >= 2 && check + sum === 11);
        } else {
            return false;
        }
    }
</script>
</body>
</html>