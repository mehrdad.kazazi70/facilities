<%@ page import="model.entity.Loan" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Filing For Customers</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <style>
        #inputLoanType, #customerNumber {
            opacity: 0;
        }
    </style>
</head>
<body id="body" onload="reload()">
<div>
    <ul class="nav nav-tabs">
        <li role="presentation"><a
                href="${pageContext.request.contextPath}/person/definitionCustomer/definitionProfile.jsp">New customer
            definition</a>
        </li>
        <li role="presentation"><a href="${pageContext.request.contextPath}/person/searchCustomers/searchCustomers.jsp">Search
            Customer</a></li>
        <li role="presentation"><a href="${pageContext.request.contextPath}/loan/definitionLoanType/loanType.jsp">Definition
            LoanType</a></li>
        <li role="presentation"><a
                href="${pageContext.request.contextPath}/loan/definitionLoanType/filing.jsp">Filing</a></li>
    </ul>
</div>
<div class="container">
    <legend>Filing For Customers</legend>
    <form action="${pageContext.request.contextPath}/findAllSingleCustomer.do" class="form-horizontal">
        <fieldset>
            <div class="col-md-4">
                <input type="text" class="form-control" name="customerNumber" placeholder="Search">
                <input type="submit" class="btn btn-primary" value="search">
            </div>
        </fieldset>
    </form>
</div>
<div class="container">
    <legend>Customer Information</legend>
    <form class="form-horizontal">
        <fieldset>
            <div class="col-md-4">
                <input id="customerName" type="text" class="form-control" value="<%=request.getAttribute("name")%>"
                       readonly>
                <input id="customerFamily" type="text" class="form-control" value="<%=request.getAttribute("family")%>"
                       readonly>
            </div>
        </fieldset>
    </form>
</div>
<br>
<br>
<br>
<form id="formAction" class="form-horizontal">
    <fieldset>
        <div id="loanTypeLists" class="container">
            <legend>Select Loan Type</legend>
            <div class="form-row align-items-center">
                <div class="col-auto my-1">
                    <select id="listOfLoanTypes" name="category">
                        <option id="value" value="<%request.getAttribute("loanList");%>"></option>
                        <option>Student</option>
                        <option>maskan</option>
                        <option>Home Loans</option>
                    </select>
                    <%--<select id="selectNumber">
                        <option>choose</option>
                    </select>--%>
                    <input id="refresh_btn" type="submit" class="btn btn-primary" name="refresh" value="refresh">
                </div>
                <br>
                <div class="col-md-4">

                    <input id="ContractPeriod" type="text" name="ContractPeriod" class="form-control" value=""
                           placeholder="minimumContractPeriod">
                    <small id="ContractPeriodHelp" class="form-text text-muted">please insert value type of
                        days.
                    </small>
                    <br>
                    <input id="ContractAmount" type="text" name="ContractAmount" class="form-control" value=""
                           placeholder="ContractAmount">
                    <small id="minimumContractAmountHelp" class="form-text text-muted">please insert value type of
                        Rials.
                    </small>
                    <br>
                    <div class="col-auto my-1">
                        <button id="submit_btn" type="submit" class="btn btn-primary">Submit</button>
                        <input name="inputLoanType" id="inputLoanType" class="form-control" value=""><input
                            id="customerNumber" name="customerNumber" class="form-control"
                            value="<%=request.getAttribute("customerNumber")%>">
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
</form>
<script>
    var body = document.getElementById("body");
    var listVal = document.getElementById("list");
    var refresh_btn = document.getElementById("refresh_btn");
    var formAction = document.getElementById("formAction");
    var list = document.getElementById("listOfLoanTypes");
    var inputLoanType = document.getElementById("inputLoanType");
    var submit_btn = document.getElementById("submit_btn");
    refresh_btn.onclick = function () {
        formAction.action = "/selectLoanType.do";
    };

    list.onchange = function () {
        inputLoanType.value = list.value;
    };
    submit_btn.onclick = function () {
        formAction.action = "/saveLoanFile.do";
    };

    function reload() {
        var status =<%=request.getAttribute("status")%>;
        switch (status) {
            case true:
                alert("data has been saved");
                break;
            case false:
                alert("Nothing was found");
                break;
        }
    }

    var select = document.getElementById("selectNumber");
    var options = listVal;
    for (var i = 0; i < options.length; i++) {
        var opt = options[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        select.appendChild(el);
    }
</script>
</body>
</html>