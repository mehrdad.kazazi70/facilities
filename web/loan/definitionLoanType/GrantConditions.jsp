<%--  Created by IntelliJ IDEA.
  User: Mehrdad Kazazi
  Date: 5/29/2020
  Time: 3:27 PM
  To change this template use File | Settings | File Templates.--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>definition grantConditions</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        * {
            font: 17px Calibri;
        }

        table {
            width: 70%;
            margin-left: 100px
        }

        table, th, td {
            border: solid 1px #DDD;
            border-collapse: collapse;
            padding: 2px 3px;
            text-align: center;
        }

        #addRow {
            margin-left: 150px;
            margin-bottom: 10px;
        }

        #bt {
            margin-left: 150px;
            margin-top: 10px;
        }

        #resultList, #loanType, #interestRate {
            opacity: 0;
        }
    </style>
</head>
<body onload="createTable()">
<div>
    <ul class="nav nav-tabs">
        <li role="presentation"><a
                href="${pageContext.request.contextPath}/person/definitionCustomer/definitionProfile.jsp">New customer
            definition</a></li>
        <li role="presentation"><a href="${pageContext.request.contextPath}/person/searchCustomers/searchCustomers.jsp">Search
            Customer</a></li>
        <li role="presentation"><a href="${pageContext.request.contextPath}/loan/definitionLoanType/loanType.jsp">Definition
            LoanType</a></li>
        <li role="presentation"><a href="${pageContext.request.contextPath}/person/searchCustomers/searchCustomers.jsp">Filing</a>
        </li>
    </ul>
    <br>
    <br>
    <br>
</div>
<p>
    <input type="button" class="btn btn-info" id="addRow" value="Add New Row" onclick="addRow()"/>
</p>
<div id="cont"></div>
<form id="form" method="get" action="saveGrantConditions.do" onsubmit="sendResult()">
    <div>
        <input id="resultList" name="returnList">
        <input class="form-group" id="loanType" name="loanType" type="text" value="<%=request.getAttribute("type")%>"/>
        <input class="form-group" id="interestRate" name="interestRate" type="text"
               value="<%=request.getAttribute("interest")%>"/>
    </div>
    </div>
    <input type="submit" class="btn btn-success" id="bt" name="result" value="Submit">
</form>
<script>
    var arrHead = ['', 'Name', 'Minimum contract period', 'Maximum contract period', 'Minimum contract amount', 'Maximum contract amount'];

    function createTable() {
        var empTable = document.createElement('table');
        empTable.setAttribute('id', 'empTable');
        var tr = empTable.insertRow(-1);
        for (var h = 0; h < arrHead.length; h++) {
            var th = document.createElement('th');
            th.innerHTML = arrHead[h];
            tr.appendChild(th);
        }
        var div = document.getElementById('cont');
        div.appendChild(empTable);
    }

    function addRow() {
        var empTab = document.getElementById('empTable');
        var rowCnt = empTab.rows.length;
        var tr = empTab.insertRow(rowCnt);
        tr = empTab.insertRow(rowCnt);
        for (var c = 0; c < arrHead.length; c++) {
            var td = document.createElement('td');
            td = tr.insertCell(c);
            if (c === 0) {
                var button = document.createElement('input');
                button.setAttribute('type', 'button');
                button.setAttribute('value', 'Remove');
                button.setAttribute('onclick', 'removeRow(this)');
                td.appendChild(button);
            } else {
                var ele = document.createElement('input');
                ele.setAttribute('type', 'text');
                ele.setAttribute('value', '');
                td.appendChild(ele);
            }
        }
    }

    function removeRow(oButton) {
        var empTab = document.getElementById('empTable');
        empTab.deleteRow(oButton.parentNode.parentNode.rowIndex);
    }

    function submit() {
        var myTab = document.getElementById('empTable');
        var arrValues = [];
        for (row = 1; row < myTab.rows.length - 1; row++) {
            for (c = 0; c < myTab.rows[row].cells.length; c++) {
                var element = myTab.rows.item(row).cells[c];
                if (element.childNodes[0].getAttribute('type') === 'text') {
                    arrValues.push("" + element.childNodes[0].value + "");
                }
            }
        }
        return arrValues;
    }

    function sendResult() {
        var resultElement = document.getElementById("resultList");
        resultElement.value = submit();
    }
</script>
</body>
</html>
