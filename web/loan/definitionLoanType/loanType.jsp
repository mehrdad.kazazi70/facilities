<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>definition Facility</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <style>
        #error {
            opacity: 0;
        }
    </style>
</head>
<body onload="reload()">
<div>
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/person/definitionCustomer/definitionProfile.jsp">New customer definition</a>
        </li>
        <li role="presentation"><a href="/person/searchCustomers/searchCustomers.jsp">Search Customer</a></li>
        <li role="presentation"><a href="/loan/definitionLoanType/loanType.jsp">Definition LoanType</a></li>
        <li role="presentation"><a href="/loan/definitionLoanType/filing.jsp">Filing</a></li>
    </ul>
</div>
<div class="container">
    <form class="form-horizontal" action="/sendLoanType.do" onsubmit="return isValid()">
        <fieldset>
            <legend>definition</legend>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="Type">Loan Type</label>
                <div class="col-md-4">
                    <input id="Type" name="Type" type="text" placeholder="Type of facility"
                           class="form-control input-md">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="interest">interest rate</label>
                <div class="col-md-4">
                    <input id="interest" name="interest" type="text" placeholder="interest rate"
                           class="form-control input-md" maxlength="2">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <input type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">
                </div>
            </div>
            <div id="error" class="alert alert-danger" style="opacity:0;transition: 0.7s" role="alert">
                <strong>Error!</strong>
            </div>
        </fieldset>
    </form>
</div>
<script>
    var submit = document.getElementById("singlebutton");
    var error_box = document.getElementsByClassName("alert alert-danger")[0];
    var typeField = document.getElementById("Type");
    var iterestField = document.getElementById("interest");

    function isValid() {
        if (typeField.value === "" || iterestField.value === "") {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'please fill the blank fields !';
            return false;
        } else if (/[^0-9]/g.test(iterestField.value)) {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'please enter only number in your interestRate field !';
            return false;
        } else if (iterestField.value.length < 2) {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'your interestRate in too short !';
            return false;
        } else if (iterestField.value === '00') {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += '  interestRate is invalid!';
            return false;
        } else if (/[0-9]/g.test(typeField.value)) {
            error_box.style.opacity = 1;
            error_box.innerHTML = ' <strong>Error!</strong>';
            error_box.innerHTML += 'please enter only letter in your loanType field !';
            return false;
        } else {
            return true;
        }
    }
    function reload() {
        var status =<%=request.getAttribute("showReportOfAction")%>;
        switch (status) {
            case true:
                alert("Information was recorded");
                break;
            case false:
                alert("invalid Data");
                break;
        }
    }
</script>
</body>
</html>