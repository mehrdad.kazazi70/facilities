package controller.loanFile.definition;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type of facility entered is received and stored in the database
 */
@WebServlet("/sendLoanType.do")
public class DefinitionLoanType extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       req.setAttribute("type",req.getParameter("Type"));
       req.setAttribute("interest",req.getParameter("interest"));
        req.getRequestDispatcher("/loan/definitionLoanType/grantConditions.jsp").forward(req,resp);
    }
}
