package controller.loanFile.definition;

import model.entity.GrantCondition;
import model.entity.Loan;
import model.entity.RealPerson;
import model.service.loanFileValidator.FilingProcessor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * The process of filing a case is done here after validation
 */
@WebServlet("/saveLoanFile.do")
public class LoanFileCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GrantCondition inputGrantCondition = new GrantCondition();
        Loan loan = new Loan();
        RealPerson realPerson = new RealPerson();
        inputGrantCondition.setMinimumContractPeriod(Long.parseLong(req.getParameter("ContractPeriod")));
        inputGrantCondition.setMinimumContractAmount(Long.parseLong(req.getParameter("ContractAmount")));
        loan.setLoanType(req.getParameter("inputLoanType"));
        String loanTypeValue = loan.getLoanType();
        realPerson.setCustomerNumber(req.getParameter("customerNumber"));
        FilingProcessor filingProcessor = new FilingProcessor();
        boolean status = filingProcessor.process(inputGrantCondition, loanTypeValue, realPerson);
        if (!status) {
            req.setAttribute("status", status);
            req.getRequestDispatcher("/loan/definitionLoanType/filing.jsp").forward(req, resp);
            return;
        }
        req.setAttribute("status", status);
        req.getRequestDispatcher("/loan/definitionLoanType/filing.jsp").forward(req, resp);
    }
}
