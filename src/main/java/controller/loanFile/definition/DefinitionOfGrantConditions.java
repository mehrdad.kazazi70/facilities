package controller.loanFile.definition;

import model.entity.GrantCondition;
import model.entity.Loan;
import model.service.loanDefinitionService.GrantConditionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The terms of grantCondition granted by the user will be sent to the database after validation
 */
@WebServlet("/saveGrantConditions.do")
public class DefinitionOfGrantConditions extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String result = req.getParameter("returnList");
        Loan loan = new Loan();
        loan.setLoanType(req.getParameter("loanType"));
        loan.setInterestRate(Long.parseLong(req.getParameter("interestRate")));
        List<String> list = new ArrayList<String>(Arrays.asList(result.split(",")));
        if (list.size() >= 5) {
            List<GrantCondition> grantConditionList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                GrantCondition grantCondition = new GrantCondition();
                grantCondition.setName(list.get(i));
                grantCondition.setMinimumContractPeriod(Long.parseLong(list.get(++i)));
                grantCondition.setMaximumContractPeriod(Long.parseLong(list.get(++i)));
                grantCondition.setMinimumContractAmount(Long.parseLong(list.get(++i)));
                grantCondition.setMaximumContractAmount(Long.parseLong(list.get(++i)));
                grantConditionList.add(grantCondition);

            }
            GrantConditionService GrantConditionService = new GrantConditionService();
            GrantConditionService.save(grantConditionList, loan);
            req.setAttribute("showReportOfAction", true);
            req.getRequestDispatcher("loan/definitionLoanType/loanType.jsp").forward(req, resp);
        } else {
            resp.sendError(708);
        }
    }
}
