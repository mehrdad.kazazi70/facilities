package controller.loanFile.search;

import model.entity.RealPerson;
import model.service.loanDefinitionService.SearchSinglePersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * The name and family is received according to the customer number entered from the database
 */
@WebServlet("/findAllSingleCustomer.do")
public class FindAllSingleCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String inputCustomerNumber = req.getParameter("customerNumber");
        SearchSinglePersonService searchSinglePersonService = new SearchSinglePersonService();
        try {
            RealPerson realPerson = searchSinglePersonService.search(inputCustomerNumber);
            req.setAttribute("name", realPerson.getName());
            req.setAttribute("family", realPerson.getFamily());
            req.setAttribute("customerNumber", realPerson.getCustomerNumber());
            req.getRequestDispatcher("/loan/definitionLoanType/filing.jsp").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
