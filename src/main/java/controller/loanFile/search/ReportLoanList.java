package controller.loanFile.search;

import model.entity.Loan;
import model.service.loanDefinitionService.SearchLoanTypeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * All kinds of facilities registered in the database for display purposes in the drop down element in UI
 */
@WebServlet("/selectLoanType.do")
public class ReportLoanList extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SearchLoanTypeService loanTypeService = new SearchLoanTypeService();
        List<Loan> loanList =loanTypeService.select();
        ArrayList<String> types = new ArrayList<>();
        for (Loan loan : loanList) {
           String type =  loan.getLoanType().toString();
            types.add(type);
        }
        types.toString();
        req.setAttribute("loanTypeList",types);
        req.getRequestDispatcher("/loan/definitionLoanType/filing.jsp").forward(req,resp);
    }
}
