package controller.realPerson;

import model.entity.RealPerson;
import model.service.realPerson.RealPersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * By pressing the deactivation button in each row, the record by referer to nationalCode status is changed to inactive
 */
public class DisableReal extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RealPerson realPerson = new RealPerson();
        realPerson.setNationalCode(req.getParameter("nationalCode"));
        RealPersonService.getInstance().delete(realPerson);
        try {
            req.setAttribute("disableRealResult", RealPersonService.getInstance().findAll(Long.valueOf(req.getParameter("nationalCode"))));
            req.getRequestDispatcher("/person/events/showRealDisable.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
