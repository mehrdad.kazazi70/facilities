package controller.realPerson;

import model.entity.RealPerson;
import model.service.formateditor.Editor;
import model.service.realPerson.RealPersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Receive input and registration information in the database after validation
 */
public class SaveReal extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RealPerson realPerson = new RealPerson();
        realPerson.setName(req.getParameter("name"));
        realPerson.setFamily(req.getParameter("family"));
        realPerson.setFatherName(req.getParameter("fatherName"));
        realPerson.setStatus(1);
        realPerson.setBirthDate(Editor.converter(req.getParameter("birthDate")));
        realPerson.setNationalCode(req.getParameter("nationalCode"));
        if (realPerson.getNationalCode().equals("0000000000")) {
            try {
                req.setAttribute("zeroStatus",false);
                req.getRequestDispatcher("/person/definitionCustomer/definitionProfile.jsp").forward(req,resp);
                throw new Exception("invalid NationalCode");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            realPerson.setCustomerNumber(Editor.randomNumberProducer(req.getParameter("nationalCode")));
            if (realPerson.getCustomerNumber().equals(String.valueOf(0))) {
                realPerson.setStatus(0);
                throw new Exception("input nationalCode is invalid");
            }
            {
                try {
                    RealPersonService.getInstance().save(realPerson);
                    req.setAttribute("result", realPerson);
                    req.getRequestDispatcher("/person/events/insertCustomer/resultRealAdd.jsp").forward(req, resp);
                } catch (Exception e) {
                    //here ****
                    try {
                        RealPerson checkList = RealPersonService.getInstance().findAll(Long.parseLong(realPerson.getNationalCode()));
                        if (checkList.getNationalCode().equals(String.valueOf(Long.parseLong(realPerson.getNationalCode()))) && checkList.getStatus() == 0) {
                            RealPersonService.getInstance().activeAccount(Long.parseLong(realPerson.getNationalCode()));
                            resp.sendError(500);
                            throw new Exception("Duplicate nationalCode");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}