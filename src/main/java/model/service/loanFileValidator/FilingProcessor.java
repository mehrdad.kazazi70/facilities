package model.service.loanFileValidator;

import model.entity.Document;
import model.entity.GrantCondition;
import model.entity.Loan;
import model.entity.RealPerson;
import model.repository.loan.GrantConditionDA;
import model.service.loanDefinitionService.DocumentRegistration;

import java.util.List;

public class FilingProcessor {
    public boolean process(GrantCondition inputGrantCondition, String loanTypeValue, RealPerson realPerson) {
        GrantConditionDA grantConditionDA = new GrantConditionDA();
        List<Loan> loanList = grantConditionDA.findAll(loanTypeValue, inputGrantCondition);
        if (loanList.size()==0){
            return false;
        }
        Document document = new Document();
        for (Loan loan : loanList) {
            document.setCustomerNumber(realPerson.getCustomerNumber());
            document.setGrantCondition_id(loan.getGrantConditionList().get(0).getId());
        }
        DocumentRegistration.save(document);
        return true;
    }
}
