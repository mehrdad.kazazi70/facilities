package model.service.realPerson;

import model.entity.RealPerson;
import model.repository.person.RealPersonDA;

import java.sql.SQLException;
import java.util.List;

public class RealPersonService {
    private static RealPersonService ourInstance = new RealPersonService();

    private RealPersonService() {
    }

    public static RealPersonService getInstance() {
        return ourInstance;
    }

    public void save(RealPerson realPerson) throws Exception {
        RealPersonDA realPersonDA = new RealPersonDA();
        realPersonDA.insert(realPerson);
    }

    public void update(RealPerson realPerson) {
        RealPersonDA realPersonDA = new RealPersonDA();
        try {
            realPersonDA.update(realPerson);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void activeAccount(Long nationalCode) {
        RealPersonDA realPersonDA = new RealPersonDA();
        try {
            realPersonDA.activeAccount(nationalCode);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(RealPerson realPerson) {
        RealPersonDA realPersonDA = new RealPersonDA();
        try {
            realPersonDA.delete(realPerson);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<RealPerson> selectAll(RealPerson realPerson) throws Exception {

        RealPersonDA realPersonDA = new RealPersonDA();
        List<RealPerson> realPersonList = realPersonDA.select(realPerson);
        RealPerson newFormatPerson = realPersonList.get(0);
        return realPersonList;

    }

    public RealPerson findAll(Long nationalCode) throws Exception {
        RealPersonDA realPersonDA = new RealPersonDA();
        return realPersonDA.findAll(nationalCode);
    }
}
