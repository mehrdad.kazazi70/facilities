package model.service.loanDefinitionService;

import model.entity.Loan;
import model.repository.loan.FindAllLoanTypeDA;

import java.util.List;

public class SearchLoanTypeService {
    public List<Loan> select(){
        FindAllLoanTypeDA loanTypeDA = new FindAllLoanTypeDA();
        return loanTypeDA.findAll();
    }
}
