package model.service.loanDefinitionService;

import model.entity.GrantCondition;
import model.entity.Loan;
import model.repository.loan.GrantConditionDA;

import java.util.List;

public class GrantConditionService {
    public void save(List<GrantCondition> grantCondition , Loan loan){
        GrantConditionDA grantConditionDA = new GrantConditionDA();
        grantConditionDA.insert(grantCondition , loan);
    }
}
