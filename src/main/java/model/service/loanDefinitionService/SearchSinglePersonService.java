package model.service.loanDefinitionService;

import model.entity.RealPerson;
import model.repository.loan.FindAllPersonDA;

import java.sql.SQLException;

public class SearchSinglePersonService {
    public RealPerson search(String customerNumber) throws SQLException {
        FindAllPersonDA findAllPersonDA = new FindAllPersonDA();

        return findAllPersonDA.findAll(customerNumber);
    }
}
