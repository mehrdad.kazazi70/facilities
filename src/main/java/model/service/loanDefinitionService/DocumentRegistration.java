package model.service.loanDefinitionService;

import model.entity.Document;
import model.entity.Loan;
import model.entity.RealPerson;
import model.repository.loan.SaveDocument;

public class DocumentRegistration {
    public static void save(Document document) {
        SaveDocument saveDocument = new SaveDocument();
        saveDocument.save(document);
    }
}
