package model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * The schema of a realPerson table was created
 */
@Entity(name = "realPerson")
@Table(name = "realperson")
public class RealPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(columnDefinition = "varchar2(20)")
    private String name;
    @Column(columnDefinition = "varchar2(20)")
    private String family;
    @Column(columnDefinition = "varchar2(20)")
    private String fatherName;
    @Column(columnDefinition = "Date")
    private Date birthDate;
    @Column(columnDefinition = "number",unique = true)
    private String nationalCode;
    @Column(columnDefinition = "varchar2(20)")
    private int status;
    @Column(columnDefinition = "varchar2(20)",nullable = false,unique = true)
    private String customerNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }
}

