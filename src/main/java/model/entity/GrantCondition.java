package model.entity;

import javax.persistence.*;
/**
 * The schema of a grantCondition table was created
 */
@Table(name = "grantCondition")
@Entity(name = "grantCondition")
public class GrantCondition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition = "varchar2(20)")
    private String name;
    @Column(columnDefinition = "number")
    private Long minimumContractPeriod;
    @Column(columnDefinition = "number")
    private Long maximumContractPeriod;
    @Column(columnDefinition = "number")
    private Long minimumContractAmount;
    @Column(columnDefinition = "number")
    private Long maximumContractAmount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMinimumContractPeriod() {
        return minimumContractPeriod;
    }

    public void setMinimumContractPeriod(Long minimumContractPeriod) {
        this.minimumContractPeriod = minimumContractPeriod;
    }

    public Long getMaximumContractPeriod() {
        return maximumContractPeriod;
    }

    public void setMaximumContractPeriod(Long maximumContractPeriod) {
        this.maximumContractPeriod = maximumContractPeriod;
    }

    public Long getMinimumContractAmount() {
        return minimumContractAmount;
    }

    public void setMinimumContractAmount(Long minimumContractAmount) {
        this.minimumContractAmount = minimumContractAmount;
    }

    public Long getMaximumContractAmount() {
        return maximumContractAmount;
    }

    public void setMaximumContractAmount(Long maximumContractAmount) {
        this.maximumContractAmount = maximumContractAmount;
    }
}
