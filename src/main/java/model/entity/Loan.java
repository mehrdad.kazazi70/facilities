package model.entity;

import javax.persistence.*;
import java.util.List;
/**
 * The schema of a loantype table was created
 */
@Table(name = "loanType")
@Entity(name = "loanType")
public class Loan {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(columnDefinition = "varchar2(20)")
    private String loanType;
    @Column(columnDefinition = "number")/*, nullable = false , unique = true*/
    private long interestRate;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "fk")
    private List<GrantCondition> grantConditionList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public long getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(long interestRate) {
        this.interestRate = interestRate;
    }

    public List<GrantCondition> getGrantConditionList() {
        return grantConditionList;
    }

    public List<GrantCondition> setGrantConditionList(List<GrantCondition> grantConditionList) {
        this.grantConditionList = grantConditionList;
        return grantConditionList;
    }
}
