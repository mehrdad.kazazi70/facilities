package model.entity;

import javax.persistence.*;
/**
 * The schema of a document table was created
 */
@Entity(name = "document")
@Table(name = "document")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(columnDefinition = "varchar2(20)")
    private String customerNumber;
    @Column(columnDefinition = "number")
    private Long grantCondition_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Long getGrantCondition_id() {
        return grantCondition_id;
    }

    public void setGrantCondition_id(Long grantCondition_id) {
        this.grantCondition_id = grantCondition_id;
    }
}
