package model.repository.loan;

import model.common.JPAProvider;
import model.entity.Document;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * The joint document between the client and the type of facility selected for the customer is made in the document table
 */
public class SaveDocument {
    public void save(Document document) {
        EntityManager entityManager = JPAProvider.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(document);
        entityTransaction.commit();
        entityManager.close();
    }
}
