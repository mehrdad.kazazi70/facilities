package model.repository.loan;

import model.common.JPAProvider;
import model.entity.GrantCondition;
import model.entity.Loan;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

/**
 * The list of conditions for granting the corresponding type is extracted from the database with a special type and intended.
 */
public class GrantConditionDA {
    public void insert(List<GrantCondition> grantCondition, Loan loan) {
        EntityManager entityManager = JPAProvider.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        loan.setGrantConditionList(grantCondition);
        entityManager.persist(loan);
        entityTransaction.commit();
        entityManager.close();
    }

    public List findAll(String loanTypeValue, GrantCondition inputGrantCondition) {
        EntityManager entityManager = JPAProvider.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select o from loanType o join fetch o.grantConditionList c where o.loanType=:p1 and c.minimumContractPeriod<=:p2 and c.maximumContractPeriod>=:p2 and c.minimumContractAmount<=:p3 and c.maximumContractAmount>=:p3");
        query.setParameter("p1",loanTypeValue);
        query.setParameter("p2",inputGrantCondition.getMinimumContractPeriod());
        query.setParameter("p3",inputGrantCondition.getMinimumContractAmount());
        return query.getResultList();
    }
}
