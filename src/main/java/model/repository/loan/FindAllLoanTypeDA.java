package model.repository.loan;

import model.common.JPAProvider;
import model.entity.Loan;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * A variety of facilities are extracted from the database
 */
public class FindAllLoanTypeDA {
    public List<Loan> findAll(){
        EntityManager entityManager= JPAProvider.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();

        Query query = entityManager.createQuery("select entity from loanType entity");
        List<Loan>loanList = query.getResultList();
        List<Loan> findAllLoanList = new ArrayList<>();
        for (Loan list:loanList){
            Loan loan = new Loan();
            loan.setLoanType(list.getLoanType());
            loan.setId(list.getId());
            findAllLoanList.add(loan);
        }
        return findAllLoanList;
    }
}
