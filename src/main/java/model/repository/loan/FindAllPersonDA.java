package model.repository.loan;

import model.common.JPAProvider;
import model.entity.RealPerson;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.sql.SQLException;

/**
 * The requested customer number is extracted from the database
 */
public class FindAllPersonDA {

    public RealPerson findAll(String customerNumber) throws SQLException {
        EntityManager entityManager = JPAProvider.getEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select entity from realPerson entity where entity.customerNumber=:p1");
        query.setParameter("p1", customerNumber);
        RealPerson realPerson = (RealPerson) query.getSingleResult();
        entityManager.close();
        return realPerson;
    }
}
