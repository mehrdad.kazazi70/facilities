package model.repository.person;


import model.common.JPAProvider;
import model.entity.RealPerson;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.List;

/**
 * Orders for database affairs are issued to the database in accordance with the requirements
 */
public class RealPersonDA {

    private static EntityManager entityManager;
    private static EntityTransaction entityTransaction;

    public void insert(RealPerson realPerson) throws SQLException {
        entityManager = JPAProvider.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(realPerson);
        entityTransaction.commit();
        entityManager.close();
    }

    public void update(RealPerson realPerson) throws SQLException {
        entityManager = JPAProvider.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select entity from realPerson entity where entity.customerNumber=:p1");
        query.setParameter("p1", realPerson.getCustomerNumber());
        RealPerson singleResult = (RealPerson) query.getSingleResult();
        singleResult.setName(realPerson.getName());
        singleResult.setFamily(realPerson.getFamily());
        singleResult.setFatherName(realPerson.getFatherName());
        singleResult.setBirthDate(realPerson.getBirthDate());
        singleResult.setNationalCode(realPerson.getNationalCode());
        entityManager.persist(singleResult);
        entityTransaction.commit();
        entityManager.close();
    }

    public void activeAccount(Long nationalCode) throws SQLException {
        entityManager = JPAProvider.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select entity from realPerson entity where entity.status=:p1 and entity.nationalCode=:p2");
        query.setParameter("p1",0);
        query.setParameter("p2",String.valueOf(nationalCode));
        RealPerson realPerson = (RealPerson)query.getSingleResult();
        realPerson.setStatus(1);
        entityManager.persist(realPerson);
        entityTransaction.commit();
        entityManager.close();
    }

    public void delete(RealPerson realPerson) throws SQLException {
        entityManager = JPAProvider.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select entity from realPerson entity where entity.nationalCode=:p1");
        query.setParameter("p1", realPerson.getNationalCode());
        RealPerson singleResult = (RealPerson) query.getSingleResult();
        singleResult.setStatus(0);
        entityManager.persist(singleResult);
        entityTransaction.commit();
        entityManager.close();
    }

    public List<RealPerson> select(RealPerson realPerson) throws Exception {
        entityManager = JPAProvider.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select entity from realPerson entity where entity.status=:p1 and (entity.name=:p2 or entity.family=:p3 or entity.nationalCode=:p4 or entity.customerNumber=:p5)");
        query.setParameter("p1", 1);
        query.setParameter("p2", realPerson.getName());
        query.setParameter("p3", realPerson.getFamily());
        query.setParameter("p4", realPerson.getNationalCode());
        query.setParameter("p5", realPerson.getCustomerNumber());
        return (List<RealPerson>) query.getResultList();
    }

    public RealPerson findAll(Long nationalCode) throws Exception {
        entityManager = JPAProvider.getEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Query query = entityManager.createQuery("select entity from realPerson entity where entity.nationalCode=:p1");
        query.setParameter("p1",String.valueOf(nationalCode));
        RealPerson realPerson = (RealPerson) query.getSingleResult();
        entityTransaction.commit();
        entityManager.close();
        return realPerson;
    }

}
