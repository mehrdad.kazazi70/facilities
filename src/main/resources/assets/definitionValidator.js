var name = document.getElementById("realName");
var family = document.getElementById("realFamily");
var fatherName = document.getElementById("realFatherName");
var birthDate = document.getElementById("realBirthDate");
var nationalCode = document.getElementById("realNationalCode");
var error_box = document.getElementsByClassName("alert alert-danger")[0];

function isValidIranianNationalCode() {
    if (name.value === "" || family.value === "" || fatherName.value === "" || birthDate.value === "" || nationalCode.value === "") {
        error_box.style.opacity = 1;
        error_box.innerHTML = ' <strong>Error!</strong>';
        error_box.innerHTML += 'please fill the blank fields !';
        return false;
    } else if (/[^0-9]/g.test(nationalCode.value)) {
        error_box.style.opacity = 1;
        error_box.innerHTML = ' <strong>Error!</strong>';
        error_box.innerHTML += 'please enter only number in your nationalCode field !';
        return false;
    } else if (nationalCode.value.length < 10) {
        error_box.style.opacity = 1;
        error_box.innerHTML = ' <strong>Error!</strong>';
        error_box.innerHTML += 'your nationalCode in too short !';
        return false;
    } else if (nationalCode.value === '0000000000' || nationalCode.value === '1111111111' || nationalCode.value === '2222222222' || nationalCode.value === '3333333333' || nationalCode.value === '4444444444' || nationalCode.value === '5555555555' || nationalCode.value === '6666666666' || nationalCode.value === '7777777777' || nationalCode.value === '8888888888' || nationalCode.value === '9999999999') {
        error_box.style.opacity = 1;
        error_box.innerHTML = ' <strong>Error!</strong>';
        error_box.innerHTML += '  nationalCode is invalid!';
        return false;
    } else if (nationalCode.value) {
        var check = parseInt(nationalCode.value[9]);
        var sum = 0;
        var i;
        for (i = 0; i < 9; ++i) {
            sum += parseInt(nationalCode.value[i]) * (10 - i);
        }
        sum %= 11;
        return (sum < 2 && check === sum) || (sum >= 2 && check + sum === 11);
    } else {
        alert("hoooooooooooo");
        return false;
    }
}
